FROM rexyai/restrserve:1.2.1

RUN mkdir /statastic
ADD src/R/statastic.R /statastic/statastic.R

RUN mkdir /statastic/static
ADD src/web/summary.html /statastic/static/summary.html

CMD ["Rscript", "/statastic/statastic.R"]